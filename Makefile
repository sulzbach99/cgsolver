SRC=			src
INCLUDE=		$(SRC)/include
USR=			usr
BUILD=			$(USR)/bin
LIB=			$(USR)/lib
ETC=			etc
DOC=			doc
EXE=			cgSolver
MFLAGS=			all

.PHONY:			debug all pre $(SRC) post doc clean

debug:			MFLAGS=debug
debug:			all

all:			pre $(SRC) post

pre:
				@mkdir -p $(BUILD) $(INCLUDE) $(LIB)

$(SRC):
				$(MAKE) $(MFLAGS) VPATH=../$(BUILD):../$(LIB) INCLUDE=../$(INCLUDE) EXE=$(EXE) --directory=$@

post:
				-@mv -f $(SRC)/*.o $(LIB)
				-@mv -f $(SRC)/$(EXE) $(BUILD)

doc:
				@mkdir -p $(DOC)
				@rm -rf $(DOC)/*
				@doxygen $(ETC)/doxygen.cfg

clean:			MFLAGS=clean
clean:			$(SRC)
				@rm -rf $(USR)
