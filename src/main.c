/**
 * @file
 * @author Lucas Sulzbach GRR20171595
 * @author Marllon Weslley Cabral Marques GRR20170149
 */
#include "common.h"
#include "cg.h"
#include "generate.h"
#include <getopt.h>
#include <string.h>
//#include <likwid.h> Library for likwid use
/**
 * Valor da tolerância caso não seja definida pelo usuário
 */
//#define DEFAULT_ERR 10e-4
#define DEFAULT_ERR 0  //para entrar no while infinito e estourar em iterações

#ifdef DEBUG
    #define MIN_SIZE 0
#else
    /**
    * Valor mínimo de n
    */
    #define MIN_SIZE 10
#endif

int main(int argc, char **argv) {
    unsigned int opt, n, k, i;
    double p, e = DEFAULT_ERR;
    char *o;
    
    char nflag = 0,
         kflag = 0,
         pflag = 0,
         iflag = 0,
         eflag = 0,
         oflag = 0;

    while ((opt = getopt(argc, argv, "n:k:p:i:e:o:")) != -1) {
        switch (opt) {
            case 'n':
                if ((n = atoi(optarg)) <= MIN_SIZE) {
                    fprintf(stderr, "Argumento 'n' deve ser maior do que 10\n");
                    exit(EXIT_FAILURE);
                }
                nflag++;
                break;
            case 'k':
                if ((k = atoi(optarg)) <= 1 || !(k % 2)) {
                    fprintf(stderr, "Argumento 'k' deve ser um número ímpar >1\n");
                    exit(EXIT_FAILURE);
                }
                kflag++;
                break;
            case 'p':
                if ((p = atof(optarg)) >= 1.0 || p < 0.0) {
                    fprintf(stderr, "Argumento 'p' deve estar contido em [0.0, 1.0[\n");
                    exit(EXIT_FAILURE);
                }
                pflag++;
                break;
            case 'i':
                i = atoi(optarg);
                iflag++;
                break;
            case 'e':
                e = atof(optarg);
                eflag++;
                break;
            case 'o':
                o = optarg;
                oflag++;
                break;
            case '?':
                exit(EXIT_FAILURE);
        }
    }

    #ifdef DEBUG
        oflag++;
    #endif

    if (!(nflag && kflag && pflag && iflag && oflag)) {
        fprintf(stderr, "Parâmetros obrigatórios faltando.\n");
        exit(EXIT_FAILURE);
    }

    FILE *output;
    double *A, *Anew, *B, *Bnew, *X, err, res, pcTime, methodTime, resTime;
    unsigned int itNum, I, J;
    nx1Size = (size_t) n * sizeof(double);
    nxnSize = (size_t) n * nx1Size;

    #ifdef DEBUG
        output = stdout;
    #else
        output = fopen(o,"w+");
        if (output == NULL)
        {
            fprintf(stderr, "Erro ao criar arquivo com os parâmetros especificados.\n");
            exit(EXIT_FAILURE);
        }
    #endif

    A = (double *) malloc(nxnSize);
    B = (double *) malloc(nx1Size);

    /* Seed utilizada na geração dos valores pseudo-aleatórios. */
    srand(20182);

    /* Preenche as k-diagonais com os valores pseudo-aleatórios */
    /* e as demais posições com 0. */
    for (I = 0; I < n; I++)
        for (J = 0 ; J < n; J++)
            A[I * n + J] = (abs(I - J) <= (k - 1) / 2)?(generateRandomA(I, J, k)):(0.0);
    
    for (I = 0; I < n; I++)
        B[I] = generateRandomB(k);

    Anew = simetricA(A, n, &pcTime); //verificar se a variavel de tempo para preparação é essa
    Bnew = simetricB(A, B, n, &pcTime); // ^^^^^^

    fprintf(output, "# ls17 LUCAS SULZBACH\n");
    //fprintf(output, "# GRR20171595\n");
    fprintf(output, "# mwcm17 MARLLON WESLLEY CABRAL MARQUES\n");
    //fprintf(output, "# GRR20170149\n");

    X = cgCalc(n, Anew, Bnew, preCondition(n, Anew, p, &pcTime), e, &err, &itNum, &methodTime, i, output);

    /* Resíduo */
    res = resCalc(n, A, B, X, &resTime); //ARRUMADO

    fprintf(output, "# residuo: %.15g\n", res);
    fprintf(output, "# Tempo PC: %.15g\n", pcTime);
    fprintf(output, "# Tempo iteracao: %.15g\n", methodTime / (double) itNum);
    fprintf(output, "# Tempo residuo: %.15g\n", resTime);
    fprintf(output, "#\n");
    fprintf(output, "%d\n", n);

    //fprintf(output, "# Valores de X:\n");
    for (I = 0; I < n; I++)
        fprintf(output, "%.15g ", X[I]);
    fprintf(output, "\n");

    fclose(output);

/* TODO
    if (itNum == i && err > e) {
        fprintf(stderr, "Método não convergiu\n");
        exit(EXIT_FAILURE);
    }
*/

    exit(EXIT_SUCCESS);
}