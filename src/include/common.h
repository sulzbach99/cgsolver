/**
 * @file
 * @author Lucas Sulzbach GRR20171595
 * @author Marllon Weslley Cabral Marques GRR20170149
 */

#ifndef __COMMON__
#define __COMMON__

#include <stdio.h>
#include <stdlib.h>

/**
 * Estas variáveis consistem nos tamanhos em bytes de uma matriz nx1 (vetor) e uma matriz nxn.
 * São declaradas globalmente pois são utilizadas em todos os arquivos do programa.
 */
size_t nx1Size, nxnSize;

#endif
