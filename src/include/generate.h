/*! \file */

#ifndef __GENERATE__
#define __GENERATE__

#include "common.h"

double generateRandomA( unsigned int i, unsigned int j, unsigned int k );
double generateRandomB( unsigned int k );

/**
 * - Função que gera os coeficientes de um sistema linear k-diagonal.
 * - i,j: coordenadas do elemento a ser calculado (0<=i,j<n).
 * - k: numero de diagonais da matriz A.
 */
inline double generateRandomA( unsigned int i, unsigned int j, unsigned int k )
{
  static double invRandMax = 1.0 / (double)RAND_MAX;
  return ((i==j)?((double)(k<<1)):(1.0))  * ((double)rand() * invRandMax);
}
/**
 * - Função que gera os termos independentes de um sistema linear k-diagonal.
 * - k: numero de diagonais da matriz A.
 */
inline double generateRandomB( unsigned int k )
{
  static double invRandMax = 1.0 / (double)RAND_MAX;
  return ((double)(k<<2)) * ((double)rand() * invRandMax);
}

#endif