/**
 * @file
 * @author Lucas Sulzbach GRR20171595
 * @author Marllon Weslley Cabral Marques GRR20170149
 */

#ifndef __MATRIX__
#define __MATRIX__

#include "common.h"
#include <stdio.h>
#include <string.h>

#ifdef DEBUG
    void prnVet(char *title, double *vet, unsigned int n);
    void prnMat(char *title, double *mat, unsigned int n);
#endif

double *nxnProduct(double *Dest, double *A, double *B, unsigned int n);
double *invertMatrix(double *Dest, double *M, unsigned int n);
double nx1Product(double *A, double *B, unsigned int n);
double *transposeMatrix(double *Mdest, double *Msrc, unsigned int n);

#ifdef DEBUG
    void prnVet(char *title, double *vet, unsigned int n) {

    fprintf(stderr, "==========================\n");
    fprintf(stderr, "%s\n", title);
    fprintf(stderr, "==========================\n\n");

    for (int i=0; i < n; ++i)
        fprintf(stderr, "%12.7lg", vet[i]);

    fprintf(stderr, "\n==========================\n\n");
    }

    void prnMat(char *title, double *mat, unsigned int n) {

    fprintf(stderr, "==========================\n");
    fprintf(stderr, "%s\n", title);
    fprintf(stderr, "==========================\n\n");

    for (unsigned int i=0; i < n; ++i) {
        for (unsigned int j=0; j < n; ++j)
        fprintf(stderr, "%12.7lg", mat[i * n + j]);

        fprintf(stderr, "\n");
    }

    fprintf(stderr, "\n==========================\n\n");
    }
#endif

/**
 * Retorna o produto de uma matriz nxn com uma matriz 1xn (vetor).
 * Dados:
 * - Vetor 1xn destino [Dest], que é um ponteiro para double.
 * - Matriz 1xn [A], que é um ponteiro para double.
 * - Vetor 1xn [B], que é um ponteiro para double.
 * - Tamanho do vetor [n], que é um inteiro positivo.
 */
inline double *nxnProduct(double *Dest, double *A, double *B, unsigned int n) {
    double aux;
    for (unsigned int i = 0; i < n; i++) {
        aux = 0.0;

        for (unsigned int j = 0; j < n; j++)
            aux += A[i * n + j] * B[j];

        Dest[i] = aux;
    }

    #ifdef DEBUG
        prnVet("nxnProduct", Dest, n);
    #endif

    return Dest;
}

/**
 * Retorna a matriz inversa de uma matriz DIAGONAL nxn.
 * Dados:
 * - Vetor 1xn destino [Dest], que é um ponteiro para double.
 * - Matriz nxn a ser invertida [M], que é um ponteiro para double.
 * - Tamanho do do vetor [n], que é um inteiro positivo.
 */
inline double *invertMatrix(double *Dest, double *M, unsigned int n) {
    unsigned int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < i; j++)
            Dest[i * n + j] = 0;

        Dest[i * n + i] = 1 / M[i * n + i];

        for (j = i + 1; j < n; j++)
            Dest[i * n + j] = 0;
    }

    #ifdef DEBUG
        prnMat("invertMatrix", Dest, n);
    #endif

    return Dest;
}

/**
 * Retorna o produto entre dois vetores de mesmo tamanho [n].
 * Dados:
 * - Vetor 1xn [A], que é um ponteiro para double.
 * - Vetor 1xn [B], que é um ponteiro para double.
 * - Tamanho do vetor [n], que é um inteiro positivo.
 */
inline double nx1Product(double *A, double *B, unsigned int n) {
    double ret = 0.0;
    for (unsigned int i = 0; i < n; i++)
        ret += A[i] * B[i];

    #ifdef DEBUG
        fprintf(stderr, "==========================\n");
        fprintf(stderr, "nx1Product: %12.7lg\n", ret);
        fprintf(stderr, "==========================\n\n");
    #endif

    return ret;
}

/**
 * Retorna a transposta de uma matriz nxn.
 * Dados:
 * - Matriz nxn destino [Mdest], que é um ponteiro para double.
 * - Matriz nxn a ser invertida [Msrc], que é um ponteiro para double.
 * - Medida do lado da matriz [n], que é um inteiro positivo.
 */
inline double *transposeMatrix(double *Mdest, double *Msrc, unsigned int n) {
    unsigned int i, j;
    for (i = 0; i < n; i++) {
        /* Atribuição dos valores em um novo vetor matriz para evitar modificar o vetor matriz original */
        for (j = 0; j < n ; j++) {
            Mdest[j*n+i] = Msrc[i*n+j];
            Mdest[i*n+j] = Msrc[j*n+i];
        }
    }

    #ifdef DEBUG
        prnMat("transposeMatrix", Mdest, n);
    #endif

    return Mdest;
}

#endif