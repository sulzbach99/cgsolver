/*! \file */

#ifndef __MYTIMESTAMP__
#define __MYTIMESTAMP__

#include "common.h"
#include <sys/time.h>

double timestamp(void);
/**
 * Função para cálculo de tempo decorrido por ações do programa.	
 */
inline double timestamp(void){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return((double)(tp.tv_sec*1000.0 + tp.tv_usec/1000.0));
}

#endif