/**
 * @file
 * @author Lucas Sulzbach GRR20171595
 * @author Marllon Weslley Cabral Marques GRR20170149
 */

#ifndef __CG__
#define __CG__

#include "common.h"
#include <stdio.h>


/**
 * Retorna a matriz M pré-condicionante a ser utilizada no método dos gradientes conjugados
 * com pré-condicionamento.
 * Dados:
 * - Medida do tamanho da matriz nxn [n], que é um inteiro positivo.
 * - Matriz nxn [A], que é um ponteiro para double.
 * - Seletor [w], que é um double.
 * Se o seletor [w] é zero, então é retornada a matriz identidade (sem pré-condicionante).
 * Caso contrário, se o seletor [w] está entre 0 e 1, então é retornada uma matriz diagonal cujos valores
 * da diagonal são os mesmos que os da diagonal da matriz A (pré-condicionador de Jacobi).
 * - Variável para medir o tempo decorrido [time], que é um ponteiro para double (referência).
 */
double *preCondition(unsigned int n,
    double *A,
    double w,
    double *time);

/**
 * Método dos gradientes conjugados com pré-condicionamento.
 * Retorna o vetor 1xn solução [X], que é um ponteiro para double.
 * Dados:
 * - Tamanho do sistema linear [n], que é um inteiro positivo.
 * - Matriz nxn SIMÉTRICA dos coeficientes do sistema linear [A], que é um ponteiro para double.
 * - Vetor 1xn com os termos independentes do sistema linear [B], que é um ponteiro para double.
 * - Matriz nxn pré-condicionante [M], que é um ponteiro para double.
 * - Tolerância para aproximação da solução [tol], que é um double.
 * - Erro aproximado [err] obtido ao calcular a norma máxima do vetor solução, que é um ponteiro para double (referência).
 * - Número de iterações realizadas pelo metódo [itNum], que é um ponteiro para inteiro positivo (referência).
 * - Tempo decorrido durante a aplicação do método [methodTime], que é um ponteiro para double (referência).
 * - Número máximo de iterações permitidas [maxIt], que é um inteiro positivo.
 */
double *cgCalc(unsigned int n,
    double *A,
    double *B,
    double *M,
    double tol,
    double *err,
    unsigned int *itNum,
    double *time,
    unsigned int maxIt,
    FILE *output);

/**
 * Retorna a norma do resíduo de dado sistema linear Ax = B.
 * Dados:
 * - Tamanho do sistema linear [n], que é inteiro positivo.
 * - Matriz nxn [A] de coeficientes do sistema linear, que é um ponteiro para double.
 * - Vetor 1xn [B] de valores independentes do sistema linear, que é um ponteiro para double.   
 * - Vetor 1xn [X] das incógnitas do sistema linear, que é um ponteiro para double.
 * - Variável para medir tempo do método [time], que é um ponteiro para double (referência).
 */
double resCalc(unsigned int n,
    double *A,
    double *B,
    double *X,
    double *time);

/**
 * Retorna a matriz [A] "preparada", ou seja, simétrica, na forma A^TA, tal como na fórmula A^TAx = A^TB.
 * Dados:
 * - Matriz nxn [A], que é um ponteiro para double.
 * - Tamanho do lado da matrix [n], que é um inteiro positivo.
 */
double *simetricA(double *A, 
    unsigned int n,
    double *time);

 /**
 * Retorna o vetor [B] "preparado", na forma A^TB, tal como na fórmula A^TAx = A^TB.
 * Dados:
 * - Matriz nxn [A], que é um ponteiro para double.
 * - Vetor 1xn [B], que é um ponteiro para double.
 * - Tamanho do lado da matrix [n], que é um inteiro positivo.
  */
double *simetricB(double *A, 
    double *B, 
    unsigned int n,
    double *time);

#endif