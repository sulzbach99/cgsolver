/**
 * @file
 * @author Lucas Sulzbach GRR20171595
 * @author Marllon Weslley Cabral Marques GRR20170149
 */

#include "cg.h"
#include "matrix.h"
#include "timestamp.h"
#include <string.h>
#include <math.h>

double *preCondition(unsigned int n,
    double *A,
    double w,
    double *time) {

    *time = timestamp();

    double *M;
    unsigned int i;

    /* Aloca com todas as posições zeradas */
    M = (double *) calloc((size_t) 1, nxnSize);

    if (w == 0)
        for (i = 0; i < n; i++)
            M[i * n + i] = 1.0;
    else
        for (i = 0; i < n; i++)
            M[i * n + i] = A[i * n + i];

    *time = timestamp() - *time;

    return M;
}


double *cgCalc(unsigned int n,
    double *A,
    double *B,
    double *M,
    double tol,
    double *err,
    unsigned int *itNum,
    double *time,
    unsigned int maxIt,
    FILE *output) {

    *time = timestamp();
    /**
     * Operações feitas na função.    
     */
    double *Minv, *X, *R, *V, *Y, *Z, aux, aux1, s, m;
    unsigned int i;

    /**
     * M^-1 
     */
    Minv = invertMatrix((double *) malloc(nxnSize), M, n);

    /** 
     * X0 = 0 
     */
    X = (double *) calloc((size_t) 1, nx1Size);

    /**
     * R = B 
     */
    R = (double *) memcpy(malloc(nx1Size), (const void *) B, nx1Size);

    /**
     * V = Minv * B 
     */
    V = nxnProduct((double *) malloc(nx1Size), Minv, B, n);

    /** 
     * Y = V 
     */
    Y = (double *) memcpy(malloc(nx1Size), (const void *) V, nx1Size);

    /**
     * aux = Y^T * R 
     */
    aux = nx1Product(Y, R, n);

    /**
     * Z = A * V 
     */
    Z = nxnProduct((double *) malloc(nx1Size), A, V, n);

    /**
     * s = aux / (V^T * Z) 
     */
    s = aux / nx1Product(V, Z, n);

    /**
     * X1 = X0 + s * V 
     */
    *err = 0.0;
    for (i = 0; i < n; i++) {
        aux1 = X[i];
        X[i] += s * V[i];
        if (fabs(aux1 - X[i]) > *err)
            *err = fabs(aux1 - X[i]);
    }
    fprintf(output, "# iter 1: %.15g\n", *err);

    /**
     * R = R - s * Z 
     */
    for (i = 0; i < n; i++)
        R[i] -= s * Z[i];

    /**
     * Y = M^-1 * R 
     */
    Y = nxnProduct(Y, Minv, R, n);

    for ((*itNum) = 1; *itNum < maxIt && *err > tol; (*itNum)++) {
        /**
         * aux1 = Y^T * R 
         */
        aux1 = nx1Product(Y, R, n);

        m = aux1 / aux;
        aux = aux1;

        /**
         * V = Y + m * V 
         */
        for (int i = 0; i < n; i++)
            V[i] = Y[i] + m * V[i];

        /** 
         * Z = A * V 
         */
        Z = nxnProduct(Z, A, V, n);

        /**
         * s = aux / (V^T * Z) 
         */
        s = aux / nx1Product(V, Z, n);

        /**
         * X1 = X0 + s * V 
         */
        *err = 0.0;
        for (i = 0; i < n; i++) {
            aux1 = X[i];
            X[i] += s * V[i];
            if (fabs(aux1 - X[i]) > *err)
                *err = fabs(aux1 - X[i]);
        }
        fprintf(output, "# iter %d: %.15g\n", (*itNum) + 1, *err);

        /**
         * R = R - s * Z 
         */
        for (i = 0; i < n; i++)
            R[i] -= s * Z[i];

        /**
         * Y = M^-1 * R 
         */
        Y = nxnProduct(Y, Minv, R, n);
    }

    free(Minv);
    free(R);
    free(V);
    free(Y);
    free(Z);

    *time = timestamp() - *time;

    return X;
}

double resCalc(unsigned int n,
    double *A,
    double *B,
    double *X,
    double *time) {

    *time = timestamp();

    double *Res, ret;

    Res = nxnProduct((double *) malloc(nx1Size), A, X, n);
    for (unsigned int i = 0; i < n; i++)
        Res[i] = B[i] - Res[i];

    ret = sqrt(nx1Product(Res, Res, n));

    free(Res);

    *time = timestamp() - *time;

    return ret;
}

double *simetricA(double *A, 
    unsigned int n, 
    double *time) {

    *time = timestamp();

    double *Aux, *Anew;
    unsigned int i, j, k;

    Aux = transposeMatrix((double *) malloc(nxnSize), A, n);

    /* Inicializada em 0 */
    Anew = (double *) calloc((size_t) 1, nxnSize);
    /* Multiplicação de duas matrizes nxn */
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            for (k = 0; k < n; k++)
                Anew[i * n + j] += Aux[i * n + k] * A[k * n + j];

    #ifdef DEBUG
        prnMat("productKMatAndKMat", Anew, n);
    #endif

    free(Aux);
    
    *time = timestamp() - *time;
    return Anew;
}

double *simetricB(double *A, 
    double *B, 
    unsigned int n, 
    double *time) {
    
    *time = timestamp();
    
    double *Aux, *Bnew;


    Aux = transposeMatrix((double *) malloc(nxnSize), A, n);
    Bnew = nxnProduct((double *) malloc(nx1Size), Aux, B, n);

    free(Aux);
    
    *time = timestamp() - *time;
    return Bnew;
}
